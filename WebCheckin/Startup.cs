﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebCheckin.Startup))]
namespace WebCheckin
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
